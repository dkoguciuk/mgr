\chapter{Projekt równoległego algorytmu RANSAC}
\label{sec::ransac_rownolegly}

	\section{Ogólny zarys}
	
		Aby przeprowadzić analizę projektowanego algorytmu należy poczynić kilka założeń wstępnych. Pierwszym z nich jest zakres użyteczności -- w niniejszej pracy w większym stopniu skupiono się na znalezieniu możliwie najlepszego dopasowania między modelem, a rzeczywistym, obserwowanym obiektem. Zakłada się więc, że chmura źródłowa jest bardzo dobrej jakości i niesie pełną informację o modelu, czyli jest dobrze zdefiniowana z każdej strony, zaś chmura docelowa jest zaszumiona poprzez nieidealny pomiar oraz może być niepełna, przysłonięta przez inne obiekty lub widoczna z jednej perspektywy.
		
		Warto również zauważyć, że niniejszy algorytm celuje w obszar zastosowań robotyki społecznej, czyli urządzeń pracujących wśród ludzi, w domu czy biurze. Pociąga to za sobą wstępną definicję modeli, z jakimi przyjdzie mu pracować. Niniejsza praca będzie więc nakierowana na rejestrację przedmiotów codziennego użytku i prawdopodobnie wymagałaby zmiany niektórych parametrów przy próbie zastosowania do dopasowywania obiektów znacznie mniejszych lub większych. 
		
		Skupiając się na tego typu aplikacjach należy zauważyć, że większość z nich używa sensorów RGB-D \cite{rgbd} takich jak Microsoft Kinect, czy Asus X'tion -- dokładną analizę różnych sensorów można znaleźć w \cite{kornuta}. Ich zaletą jest bardzo niska cena w stosunku do jakości otrzymanej chmury punktów, tak więc obiekty, które będą poddawane rejestracji przez niniejszy algorytm będą zbudowane z od kilkuset do kilku tysięcy punktów. Pociąga to za sobą ważne następstwo w postaci nieco innej struktury algorytmu. W standardowej wersji, po wylosowaniu trzech punktów chmury źródłowej najbliższy sąsiad w przestrzeni cech jest liczony na bieżąco, ponieważ w każdej iteracji może być znaleziona poprawna transformacja, która zakończy działanie algorytmu. W wersji równoległej nie będzie takiej możliwości, ponieważ wszystkie iteracje będą wykonywane ideologicznie w tym samym czasie, dodatkowo zakładana liczba generowanych hipotez będzie sięgała kilkunastu, może nawet kilkudziesięciu tysięcy, przez co znacznie wzrasta prawdopodobieństwo kilkukrotnego użycia tego samego punktu, zatem i kilkukrotnego szukania najbliższego sąsiada. Wobec powyższego rozumowania szukanie najbliższych sąsiadów lepiej jest przeprowadzić raz, przed rozpoczęciem właściwego algorytmu RANSAC, po to by nie wykonywać kilkukrotnie tej samej pracy. Jak pokazano na rysunku \ref{fig::ransac_par} szukanie najbliższych sąsiadów w przestrzeni cech jest pierwszym krokiem równoległej wersji algorytmu.
		
		\begin{figure}[h]
			\centering
			\begin{tikzpicture}[->,>=stealth']
				\tikzset{
					io/.style={
						circle,
						draw=black, very thick,
						minimum height=0.5cm,
						minimum width=1cm,
						text centered,
						font = \tiny,
					},
					op/.style={
						rectangle,
						rounded corners,
						draw=black, very thick,
						minimum height=1.2cm,
						text centered,
						text width=3.5cm,
						font = \tiny,
					},
					con/.style={
						rectangle,
						rotate=45,
						draw=black, very thick,
						minimum height=1.5cm,
						minimum width=1.5cm,
						font = \tiny,
					},
				}
			
			%START=======================================
			\node [io] (START) {\tiny START};
			
			%LOSOWANIE===================================
			\node [
				op,
				node distance=0cm,
				right of=START,
				yshift=-2cm,
			] (SASIEDZI) {Obliczenie najbliższych punktów w przestrzeni cech};
		
			%ZNALEZIENIE=================================
			\node [
				op,
				node distance=0cm,
				right of=SASIEDZI,
				yshift=-1.5cm
			] (GENEROWANIE) {Generowanie hipotez wraz z ich wstępną rejekcją};
		
			%OSZACOWANIE=================================
			\node [
				op,
				node distance=0cm,
				right of=GENEROWANIE,
				yshift=-1.5cm
			] (PRZYGOTOWANIE) {Przygotowanie danych w zwartej formie};
		
			%TRANSFORMACJA===============================
			\node [
				op,
				node distance=0cm,
				right of=PRZYGOTOWANIE,
				yshift=-1.5cm
			] (TESTTDD) {Zaawansowany test $T_{d,d}$ oraz dalsze odrzucenie niepoprawnych hipotez};
		
			%OBLICZENIE==================================
			\node [
				op,
				node distance=0cm,
				right of=TESTTDD,
				yshift=-1.5cm
			] (PRZYGOTOWANIE2) {Końcowe znalezienie najlepszej transformacji};
			
			
			
			%STOP========================================
			\node [
				io,
				node distance=0cm,
				right of=PRZYGOTOWANIE2,
				shift = {(0cm,-2cm)}
			] (STOP) {STOP};
			
			%PIERWSZY STOPIEN=============================
			\draw[->] (START) to (SASIEDZI);
			\draw[->] (SASIEDZI) to (GENEROWANIE);
			\draw[->] (GENEROWANIE) to (PRZYGOTOWANIE);
			\draw[->] (PRZYGOTOWANIE) to (TESTTDD);
			\draw[->] (TESTTDD) to (PRZYGOTOWANIE2);
			\draw[->] (PRZYGOTOWANIE2) to (STOP);
			
			\end{tikzpicture}
			\caption{Schemat blokowy równoległej wersji algorytmu RANSAC w zastosowaniu w dopasowywaniu chmur punktów}
			\label{fig::ransac_par}
		\end{figure}
		
		Drugim ważnym punktem jest generowanie hipotez. Tak jak w wersji szeregowej polega to na wylosowaniu trzech punktów chmury z źródłowej oraz wykorzystaniu przygotowanej we wcześniejszym kroku informacji o najbliższych im punktach w chmurze docelowej w celu estymacji transformacji. Łatwo jest tutaj wprowadzić wstępne odrzucenie hipotezy na podstawie przestawionej w rozdziale \ref{sec::poly} metody podobieństwa trójkątów.
		
		Wstępne odrzucenie hipotezy w wersji szeregowej powodowało szybsze przejście do kolejnej iteracji algorytmu. W równoległym podejściu wiąże się to z faktem, że nie wszystkie wątki będą generować hipotezy. Dalsza równoległa praca na pamięci GPU, z której duża część jest nieużywana i pusta byłaby zdecydowanie nieekonomiczna z punktu widzenia czasu wykonywania i zajętości pamięci, dlatego istnieje potrzeba przekształcenia pamięci z dużą liczbą wolnych bajtów do skondensowanej, gęstej formy i o tym opowiada krok trzeci schematu pokazanego na rysunku \ref{fig::ransac_par}.
		
		W kroku czwartym zamiast przeprowadzać pełną weryfikację hipotezy poprzez transformację całej chmury źródłowej dokonywany jest przedstawiony w rozdziale \ref{sec::testtdd} test $T_{d,d}$. W obecnej implementacji dla każdej hipotezy sprawdzane są 32 punkty chmury źródłowej i jeżeli nie są one punktami przystającymi po zastosowaniu sprawdzanej transformacji w takim stopniu jak przyjęta byłaby hipoteza podczas sprawdzenia całej chmury, to jest ona odrzucana.
		
		W ostatnim punkcie następuje ostateczne sprawdzenie każdej transformacji, która pozytywnie przeszła test $T_{d,d}$. Jako, że zakłada się, że nieodrzuconych hipotez jest w tym momencie tylko kilka, dokonuje się tego w tradycyjny sposób, czyli transformacją całej źródłowej chmury punktów oraz sprawdzeniem liczby punktów przystających. Krok ten, tak samo jak wszystkie powyżej zostanie opisany dokładnie w dalszej części dokumentu.
		
	\section{Najbliżsi sąsiedzi w przestrzeni cech}
	\label{sec::neighbors_parallel}
	
		Jak zostało opisane w rozdziale \ref{sec::neighbors_serial} do szukania punktów odpowiadających w dwóch chmurach stosuje się specjalne struktury danych umożliwiające szybkie przeszukiwanie. Problemem jest fakt, że nie ma obecnie dostępnej równoległej implementacji o otwartym źródle drzewa kd dla większej liczby wymiarów niż 3 i jest to na tyle skomplikowany problem, że mógłby stanowić zupełnie nowy temat na pracę dyplomową. W związku z tym, autor niniejszej pracy zdecydował się na implementację naiwnego przeszukiwania, gdzie sprawdzana jest odległość każdego punktu chmury docelowej dla każdego punktu chmury źródłowej i zapamiętywany jest indeks o najmniejszej obliczonej wartości. Sama odległość między dwoma wektorami cech FPFH jest liczona jako zwykła odległość euklidesowa w 33-wymiarowej przestrzeni.
		
		Biorąc pod uwagę model programistyczny architektury CUDA oraz założenia wstępne dotyczące liczby punktów opisujących model i obiekt rzeczywisty, optymalnym rozwiązaniem wydaje się podzielenie problemu w taki sposób, by jeden blok wątków pracował nad znalezieniem najbliższego sąsiada dla jednego punktu chmury źródłowej. Jest to uzasadnione koniecznością wzajemnej komunikacji wewnątrz bloku w celu znalezienia minimalnej wartości odległości. Przy tak przyjętym sposobie rozwiązania problemu jądro obliczeniowe będzie wykonywane z taką liczbą bloków, ile jest punktów w chmurze źródłowej oraz z liczbą wątków w bloku o wartości $min(|T|,1024)$. Wyrażenie to jest podyktowane z jednej strony maksymalną liczbą wątków w bloku równą 1024 dla najnowszych układów NVidia GPU (architektura Fermi oraz Kepler), a z drugiej samą liczbą punktów chmury docelowej -- jeżeli jest ona większa od 1024 to każdy wątek wewnątrz bloku będzie obliczał odległość od kilku punktu chmury docelowej, w zależności od tego jaką wielokrotnością liczby 1024 jest moc chmury docelowej (zaokrągloną w górę). Na rysunku \ref{fig::neighbors} objaśniono zasadę działania tej części algorytmu dla przypadku $|T|>1024$ (drugi przypadek jest trywialny).
		
		\begin{figure}[h]
			\centering
			\begin{tikzpicture}
			
			\tikzset{>=latex}

			% My definitions
			\def\size{0.75cm}
			\def\rect(#1,#2){\draw (#1-\size/2,#2-\size/2) rectangle +(\size,\size);}
			\def\points(#1,#2){
				\fill (#1-\size/2,#2) circle[radius=1pt];
				\fill (#1,#2) circle[radius=1pt];
				\fill (#1+\size/2,#2) circle[radius=1pt];
			}
			\def\bracket(#1,#2,#3,#4){
				\draw [
				    thick,
				    decoration={
				        brace,
				        mirror,
				        raise=#3,
				        amplitude=10pt
				    },
				    decorate
				] (#1-\size/2,0) -- (#2+\size/2,0) node [midway,yshift=-#3-\size] {#4};
			}

			%POSREDNIE
			\rect(0*1.5*\size, 0*\size)
			\rect(1*1.5*\size, 0*\size)
			\rect(2*1.5*\size, 0*\size)
			\points(3*1.5*\size, 0*\size)
			\rect(4*1.5*\size, 0*\size)
			\bracket(0*1.5*\size, 4*1.5*\size, 1.5*\size/2, 1024)
			
			\rect(5*1.5*\size, 0*\size)
			\points(6*1.5*\size, 0*\size)
			\rect(7*1.5*\size, 0*\size)
			\bracket(5*1.5*\size, 7*1.5*\size, 1.5*\size/2, 1024)
			
			\points(8*1.5*\size, 0*\size)
			
			\rect(9*1.5*\size, 0*\size)
			\points(10*1.5*\size, 0*\size)
			\rect(11*1.5*\size, 0*\size)
			\bracket(9*1.5*\size, 11*1.5*\size, 1.5*\size/2, 1024)
			
			\draw[thick] (0*1.5*\size, 1.5*\size) -- (7*1.5*\size+\size/2, 1.5*\size);
			\points(8*1.5*\size, 1.5*\size)
			\draw[thick] (9*1.5*\size-\size/2, 1.5*\size) -- (9*1.5*\size, 1.5*\size);
			\draw[->, thick] (0*1.5*\size, 1.5*\size) -- (0*1.5*\size, 0.5*\size);
			\draw[->, thick] (5*1.5*\size, 1.5*\size) -- (5*1.5*\size, 0.5*\size);
			\draw[->, thick] (9*1.5*\size, 1.5*\size) -- (9*1.5*\size, 0.5*\size);
			\node at (9/2*1.5*\size, 2*\size) {Punkty analizowane przez jeden wątek};
			
			\end{tikzpicture}
			\caption{Schemat obliczenia najbliższego sąsiada dla jednego punktu chmury źródłowej (jednego bloku wątków). Każdy z 1024 punktów ma do przeanalizowania tyle punktów, ile wielokrotności (zaokrąglonych w górę) liczby 1024 mieści się w mocy chmury docelowej. Wyniki pośrednie, czyli tablica 1024 najbliższych indeksów są zapisywane w pamięci współdzielonej dla bloku.}
			\label{fig::neighbors}
		\end{figure}
		
		Po opisanym powyżej kroku następuje konieczność znalezienia minimalnej wartości odległości wśród tablicy 1024 liczb zmiennoprzecinkowych zapisanych w pamięci współdzielonej dla bloku. Taka operacja jest jednym z tzw. prymitywów obliczeń równoległych (ang. primitive parallel operations) i jest nazywana redukcją (ang. reduce). Jest to znana i dobrze opisana cegiełka \cite{primitives}, z której buduje się zaawansowane algorytmy równoległe i polega ona na znalezieniu jednego elementu opisanego \textbf{operacją} spośród dużego zbioru danych wejściowych. Operacja musi być działaniem łącznym i dwuargumentowym, czyli możliwa jest redukcja poprzez dodawanie (znalezienie sumy elementów wejściowych), za to dzielenie nie jest już możliwe, ponieważ nie jest działaniem łącznym. W tym przypadku będzie to znalezienie minimum, co jest zarówno operacją dwuargumentową. Idea operacji redukcji równoległej jest pokazana na rysunku \ref{fig::reduce}.
	
		\begin{figure}[h]
			\centering
			\begin{tikzpicture}
			
			\tikzset{>=latex}

			% My definitions
			\def\size{0.75cm}
			\def\rect(#1,#2){\draw (#1-\size/2,#2-\size/2) rectangle +(\size,\size);}
			\def\points(#1,#2){
				\fill (#1-\size/2,#2) circle[radius=1pt];
				\fill (#1,#2) circle[radius=1pt];
				\fill (#1+\size/2,#2) circle[radius=1pt];
			}
			\def\bracket(#1,#2,#3,#4){
				\draw [
				    thick,
				    decoration={
				        brace,
				        mirror,
				        raise=#3,
				        amplitude=10pt
				    },
				    decorate
				] (#1-\size/2,0) -- (#2+\size/2,0) node [midway,yshift=-#3-\size] {#4};
			}

			%KROK 1
			\rect(0*1.5*\size, 0*\size)
			\rect(1*1.5*\size, 0*\size)
			\points(2*1.5*\size, 0*\size)
			\rect(3*1.5*\size, 0*\size)
			
			\rect(4*1.5*\size, 0*\size)
			\points(5*1.5*\size, 0*\size)
			\rect(6*1.5*\size, 0*\size)
			
			\bracket(0*1.5*\size, 3*1.5*\size, 1.5*\size/2, 512)
			\bracket(0*1.5*\size, 6*1.5*\size, 4.5*\size/2, 1024)
			
			
			\draw[thick] (0*1.5*\size, 1.5*\size) -- (4*1.5*\size, 1.5*\size);
			\draw[->, thick] (0*1.5*\size, 1.5*\size) -- (0*1.5*\size, 0.5*\size);
			\draw[->, thick] (4*1.5*\size, 1.5*\size) -- (4*1.5*\size, 0.5*\size);
			\node at (4/2*1.5*\size, 2*\size) {Wartości analizowane przez jeden wątek};
			
			%KROK 2
			\rect(0*1.5*\size, -7*\size)
			\rect(1*1.5*\size, -7*\size)
			\points(2*1.5*\size, -7*\size)
			\rect(3*1.5*\size, -7*\size)
			
			\rect(4*1.5*\size, -7*\size)
			\points(5*1.5*\size, -7*\size)
			\rect(6*1.5*\size, -7*\size)
			
			\bracket(0*1.5*\size, 3*1.5*\size, 15.5*\size/2, 256)
			\bracket(0*1.5*\size, 6*1.5*\size, 18.5*\size/2, 512)
			
			
			\draw[thick] (0*1.5*\size, -5.5*\size) -- (4*1.5*\size, -5.5*\size);
			\draw[->, thick] (0*1.5*\size, -5.5*\size) -- (0*1.5*\size, -6.5*\size);
			\draw[->, thick] (4*1.5*\size, -5.5*\size) -- (4*1.5*\size, -6.5*\size);
			\node at (4/2*1.5*\size, -5*\size) {Wartości analizowane przez jeden wątek};
			
			%OPIS
			\node [rotate=90] at (7.75*1.5*\size,0) {Krok 1};
			\draw[<->,thick] (8*1.5*\size, 2.5*\size) -- (8*1.5*\size, -3.5*\size);
			\node [rotate=90] at (7.75*1.5*\size, -7*\size) {Krok 2};
			\draw[<->,thick] (8*1.5*\size, -4.5*\size) -- (8*1.5*\size, -10.5*\size);
			
			\end{tikzpicture}
			\caption{Schemat pierwszych dwóch kroków redukcji wektora wejściowego składającego się z 1024 wartości. W każdym z dziesięciu kroków aktywnych wątków jest dwa razy mniej niż w poprzednim (zaczynając od 512).}
			\label{fig::reduce}
		\end{figure}
		
		Implementację szukania najbliższych sąsiadów pokazano w dodatku A.
	
	\section{Generowanie hipotez wraz z testem trójkąta}
	
		Dysponując wektorem indeksów najbliższych sąsiadów dla każdego z punktów chmury źródłowej można przejść do pierwszego właściwego kroku równoległego algorytmu RANSAC, czyli generowania hipotez. Polega to na wylosowaniu 3 punktów chmury źródłowej, wydobyciu odpowiadającym im punktów chmury docelowej oraz na tej podstawie oszacowaniu macierzy transformacji, tak jak to opisano w szeregowej wersji algorytmu w rozdziale \ref{sec::ransac_szeregowy}.		
		
		Zanim jednak nastąpi estymacja transformacji można przeprowadzić wstępną weryfikację hipotezy za pomocą mechanizmu podobieństwa trójkątów i wygląda ona analogicznie jak w wersji szeregowej opisanej w rozdziale \ref{sec::poly}.
		
		Elementem wymagającym szerszego komentarza może być tutaj sposób wywołania jądra obliczeniowego. Każdy wątek jest zupełnie niezależny od innych i wynikiem jego pracy jest informacja o odrzuceniu hipotezy, albo gotowa do dalszej pracy macierz transformacji. Nie ma zatem żadnych narzuconych wytycznych co ilości wątków, bloków i ich ułożenia w pamięci, dlatego kierując się ogólnymi zasadami dobrego programowania równoległego przyjęta została maksymalna liczba wątków w bloku, czyli 1024, oraz 16 bloków obliczeniowych. Dzięki temu wstępnie generowana liczba hipotez to $N = 16 \cdot 1024 = 16384$.
		
	\section{Przygotowanie danych w zwartej formie}
	
		Po zakończeniu części algorytmu opisanej w poprzednim rozdziale rzeczywista liczba wygenerowanych hipotez jest zdecydowanie mniejsza od tej początkowo zakładanej poprzez zastosowanie wstępnej ich weryfikacji. W zależności od przyjętej wartości progu podobieństwa trójkątów pozostała do dalszej pracy liczba hipotez może być ograniczona nawet o $99\%$, dlatego istnieje konieczność upakowania dużego, rzadkiego wektora liczb zmiennoprzecinkowych oznaczającego wygenerowane transformacje do zwięzłej formy.
		
		Problem zamiany wektorów o dużej liczbie zer do zwięzłej niezerowej formy jest również tak jak redukcja, jednym z podstawowych cegiełek budujących zaawansowane algorytmy równoległe. Posłużono się tutaj dwoma prymitywami, z których pierwszy polega na przydzieleniu każdej poprawnej transformacji odpowiedniego miejsca w pamięci w nowo tworzonym, zwięzłym wektorze danych. Działanie takie nazywane jest operacją skanowania (ang. scan) a ujmując dokładniej skanowania eksluzywnego (ang. exclusive), czyli takiego, w którym adres przydziela się od zera. Zasada działania tego prymitywu została pokazana na rysunku \ref{fig::scan}.
		
		\begin{figure}[h]
			\centering
			\begin{tikzpicture}
			
			\tikzset{>=latex}

			% My definitions
			\def\size{0.75cm}
			\def\rect(#1,#2,#3)
			{
				\draw (#1-\size/2,#2-\size/2) rectangle +(\size,\size);
				\node at (#1,#2) {#3};
			}
			\def\points(#1,#2){
				\fill (#1-\size/2,#2) circle[radius=1pt];
				\fill (#1,#2) circle[radius=1pt];
				\fill (#1+\size/2,#2) circle[radius=1pt];
			}
			\def\bracket(#1,#2,#3,#4){
				\draw [
				    thick,
				    decoration={
				        brace,
				        mirror,
				        raise=#3,
				        amplitude=10pt
				    },
				    decorate
				] (#1-\size/2,0) -- (#2+\size/2,0) node [midway,yshift=-#3-\size] {#4};
			}

			%POSREDNIE
			\node at (-2,0) {wejście};
			\rect(0*1.5*\size, 0*\size,$T_{0}$)
			\rect(1*1.5*\size, 0*\size,$T_{1}$)
			\rect(2*1.5*\size, 0*\size,$T_{2}$)
			\rect(3*1.5*\size, 0*\size,-)
			\rect(4*1.5*\size, 0*\size,$T_{4}$)
			\rect(5*1.5*\size, 0*\size,-)
			\rect(6*1.5*\size, 0*\size,-)
			\rect(7*1.5*\size, 0*\size,$T_{7}$)
			\points(8*1.5*\size, 0*\size)
			\rect(9*1.5*\size, 0*\size,$T_{N}$)

			%POSREDNIE
			\node at (-2,-2*\size) {wyjście};
			\rect(0*1.5*\size, -2*\size,0)
			\rect(1*1.5*\size, -2*\size,1)
			\rect(2*1.5*\size, -2*\size,2)
			\rect(3*1.5*\size, -2*\size,3)
			\rect(4*1.5*\size, -2*\size,3)
			\rect(5*1.5*\size, -2*\size,4)
			\rect(6*1.5*\size, -2*\size,4)
			\rect(7*1.5*\size, -2*\size,4)
			\points(8*1.5*\size, -2*\size)
			\rect(9*1.5*\size, -2*\size,M)
			
			%POSREDNIE
			\draw[->] (0*1.5*\size, -0.55*\size) -- (0*1.5*\size, -1.45*\size);
			\draw[->] (1*1.5*\size, -0.55*\size) -- (1*1.5*\size, -1.45*\size);
			\draw[->] (2*1.5*\size, -0.55*\size) -- (2*1.5*\size, -1.45*\size);
			\draw[->] (4*1.5*\size, -0.55*\size) -- (4*1.5*\size, -1.45*\size);
			\draw[->] (7*1.5*\size, -0.55*\size) -- (7*1.5*\size, -1.45*\size);
			\draw[->] (9*1.5*\size, -0.55*\size) -- (9*1.5*\size, -1.45*\size);

			
			\end{tikzpicture}
			\caption{Ideowy schemat operacji ekskluzywnego skanowania wektora wejściowego. Transformacja o numerze zero ma przypisany adres zero, zaś transformacja o numerze siede ma przypisany adres cztery, ponieważ to piąta poprawna transformacja w licząc od początku wektora oraz indeksując od zera.}
			\label{fig::scan}
		\end{figure}
		
		Operacja skanowania jest dość prosta w realizacji dzięki mechanizmom umożliwiającym współpracę między wątkami należącymi do tego samego bloku, jednak w przypadku skanowania wektora dłuższego niż wielkość bloku (1024 w ramach architektury fermi oraz kepler) należy użyć wariantu skanowania segmentowanego. Zarówno zasada działania operacji skanowania eksluzywnego, segmentowanego jak i optymalna jego implementacja zostały opowiedziane dokładnie w \cite{scan}. 
		
		Po przeprowadzeniu operacji skanowania dostępny jest wektor transformacji o długości $N$ o $M$ niezerowych wartościach oraz wektor również o długości $N$ adresów przydzielonych do każdej poprawnej transformacji, gdzie ostatni wyraz ma wartość $M-1$. Teraz należy przeprowadzić właściwe przenoszenie danych w taki sposób, by niezerowe transformacje były ułożone po kolei w pamięci urządzenia i jest to kolejny prymityw programowania równoległego i nazywa się operacją składania (ang. compact), co zostało pokazane schematycznie na rysunku \ref{fig::compact}.
		
		\begin{figure}[h]
			\centering
			\begin{tikzpicture}
			
			\tikzset{>=latex}

			% My definitions
			\def\size{0.75cm}
			\def\rect(#1,#2,#3)
			{
				\draw (#1-\size/2,#2-\size/2) rectangle +(\size,\size);
				\node at (#1,#2) {#3};
			}
			\def\points(#1,#2){
				\fill (#1-\size/2,#2) circle[radius=1pt];
				\fill (#1,#2) circle[radius=1pt];
				\fill (#1+\size/2,#2) circle[radius=1pt];
			}
			\def\bracket(#1,#2,#3,#4){
				\draw [
				    thick,
				    decoration={
				        brace,
				        mirror,
				        raise=#3,
				        amplitude=10pt
				    },
				    decorate
				] (#1-\size/2,0) -- (#2+\size/2,0) node [midway,yshift=-#3-\size] {#4};
			}

			%POSREDNIE
			\node at (-2,0) {wejście};
			\rect(0*1.5*\size, 0*\size,$T_{0}$)
			\rect(1*1.5*\size, 0*\size,$T_{1}$)
			\rect(2*1.5*\size, 0*\size,$T_{2}$)
			\rect(3*1.5*\size, 0*\size,-)
			\rect(4*1.5*\size, 0*\size,$T_{4}$)
			\rect(5*1.5*\size, 0*\size,-)
			\rect(6*1.5*\size, 0*\size,-)
			\rect(7*1.5*\size, 0*\size,$T_{7}$)
			\points(8*1.5*\size, 0*\size)
			\rect(9*1.5*\size, 0*\size,$T_{N}$)

			%POSREDNIE
			\node at (-2,-2*\size) {wejście};
			\rect(0*1.5*\size, -2*\size,0)
			\rect(1*1.5*\size, -2*\size,1)
			\rect(2*1.5*\size, -2*\size,2)
			\rect(3*1.5*\size, -2*\size,3)
			\rect(4*1.5*\size, -2*\size,3)
			\rect(5*1.5*\size, -2*\size,4)
			\rect(6*1.5*\size, -2*\size,4)
			\rect(7*1.5*\size, -2*\size,4)
			\points(8*1.5*\size, -2*\size)
			\rect(9*1.5*\size, -2*\size,M)
			
			%POSREDNIE
			\node at(0*1.5*\size, -1.0*\size) {+};
			\node at(1*1.5*\size, -1.0*\size) {+};
			\node at(2*1.5*\size, -1.0*\size) {+};
			\node at(4*1.5*\size, -1.0*\size) {+};
			\node at(7*1.5*\size, -1.0*\size) {+};
			\node at(9*1.5*\size, -1.0*\size) {+};
			
			%POSREDNIE
			\node at (-2,-4*\size) {wyjście};
			\rect(0*1.5*\size, -4*\size,$T_{0}$)
			\rect(1*1.5*\size, -4*\size,$T_{1}$)
			\rect(2*1.5*\size, -4*\size,$T_{2}$)
			\rect(3*1.5*\size, -4*\size,$T_{4}$)
			\rect(4*1.5*\size, -4*\size,$T_{7}$)
			\points(5*1.5*\size, -4*\size)
			\rect(6*1.5*\size, -4*\size,$T_{N}$)
			
			%POSREDNIE
			\draw[->] (0*1.5*\size, -2.55*\size) -- (0*1.5*\size, -3.45*\size);
			\draw[->] (1*1.5*\size, -2.55*\size) -- (1*1.5*\size, -3.45*\size);
			\draw[->] (2*1.5*\size, -2.55*\size) -- (2*1.5*\size, -3.45*\size);
			\draw[->] (4*1.5*\size, -2.55*\size) -- (3*1.5*\size, -3.45*\size);
			\draw[->] (7*1.5*\size, -2.55*\size) -- (4*1.5*\size, -3.45*\size);
			\draw[->] (9*1.5*\size, -2.55*\size) -- (6*1.5*\size, -3.45*\size);
			
			\end{tikzpicture}
			\caption{Ideowy schemat operacji składania rzadkiego wektora wejściowego według przygotowanych adresów każdego niezerowego elementu.}
			\label{fig::compact}
		\end{figure}
		
	\section{Zaawansowany test \boldmath{$T_{d,d}$}}
	
		Na tym etapie algorytmu nieodrzuconych hipotez jest nadal około kilkuset, dlatego istnieje potrzeba przeprowadzenia dodatkowego testu przed transformacją całej chmury punktów. Według przeprowadzonego w rozdziale \ref{sec::extended} przeglądu najnowszych metod najłatwiejsza w implementacji i najbardziej efektywna wydaje się możliwość przeprowadzania testy $T_{d,d}$.
		
		Korzystając z wiedzy na temat paczek wątków (rozdział \ref{sec::model}) jako liczbę punktów, dla których będzie przeprowadzany test przyjęto $d = 32$. Każdy wątek będzie miał za zadanie wylosować jeden punkt z modelu i przetransformować go wedle sprawdzanej hipotezy. Jeżeli chodzi o parametry wywołania jądra obliczeniowego to należy zauważyć, że jedna paczka wątków będzie pracowała nad sprawdzeniem jednej z M hipotez, dlatego wywołanych zostanie M/32 bloków, w których sprawdzane będą 32 hipotezy po 32 punkty na każdą hipotezę ($32 \cdot 32 = 1024$).
		
		Sprawdzenie, czy powyższe punkty są punktami przystającym zostało wyciągnięte poza omawiane jądro obliczeniowe z tego względu, że jest dostępne, szybkie narzędzie o otwartym źródle przeznaczone do takich zadań. Mowa tutaj o drzewie ósemkowym i jego implementacji w bibliotece PCL (ang. Point Cloud Library) \cite{pcl}. Jest to sposób przechowywania informacji o punktach w przestrzeni 3D polegający na podziale przestrzeni na mniejsze regularne części (najczęściej sześciany). Pozwala to zaoszczędzić pamięć w mniej ciekawych miejscach jednocześnie dobrze odwzorowując szczegóły obiektów. Metoda sprawdzania, czy przetransformowany punkt chmury źródłowej ma w swoim otoczeniu jakikolwiek punkt chmury docelowej oparta na drzewie ósemkowym jest uzasadniona, ponieważ sprawdzanych będzie nawet kilka, czy kilkanaście tysięcy punktów. Przeszukiwanie drzewa ósemkowego jest bardzo szybkie -- budowanie zajmuje więcej czasu, ale będzie wykonywane dla chmury docelowej tylko raz.
		
		Znów odrzucane były transformacje, dlatego z wektora wszystkich teraz sprawdzanych hipotez należy wyciągnąć tylko te, którze poprawnie przeszły test $T_{d,d}$. Zakłada się, że ich libcza będzie bardzo niewielka (rzędu kilku, kilkunastu hipotez), dlatego zamiast zaawansowanych sposobów pokazanych w poprzednim rozdziale przygotowania danych w zwartej formie, autor niniejszej pracy zdecydował się na składanie bazując na mechaniźmie operacji atomowych -- inkrementowanie adresu zapisanej transformacji z blokowaniem dostępu do tego adresu. Znacznie uproszcza to kod źródłowy i pozytywnie wpływa na czas wykonywania programu w przypadku, kiedy liczba hipotez, które przeszła poprawnie test $T_{d,d}$ jest niewielka.
		
	\section{Końcowe znalezienie najlepszej transformacji}
	
		Ostatnim punktem równoległego algorytmu RANSAC prezentowanego w tej pracy jest finalne znalezienie najlepszej transformacji. Polega ono na tym samym, co zwykłe sprawdzenie hipotezy w wersji szeregowej, czyli transformacji całej chmury źródłowej wedle sprawdzanej hipotezy (tylko tych, które przeszły test $T_{d,d}$). Realizowane jest to w bardzo podobny sposób do tego opisanego w poprzednim rozdziale, z tą różnicą, że dla każdej hipotezy transformujemy wszystkie punkty chmury źródłowej.
		
		Sprawdzenie, czy transformowane punkty są punktami przystającymi realizowane jest w dokładnie ten sam sposób, czyli za pomocą już zbudowanego i wykorzystywanego drzewa ósemkowego. Inaczej zaś jest przeprowadzone finalne jądro obliczeniowe, które wybiera z wszystkich poprawnych hipotez tą, która ma najwięcej punktów przystających. Jest to inne podejście niż w przypadku szeregowej wersji, ma jednak pełne uzasadnienie, ponieważ na końcu może być dostępnych kilka poprawnych transformacji i wybranie akurat tej, która będzie pierwsza kolejności w wektorze byłoby bezsensowną stratą jakości.
		
		Jako wynik algorytmu wyświetlana jest informacja o braku sukcesu przeprowadzonego dopasowania algorytmem RANSAC albo w przypadku powodzenia zwrócona jest najlepsza znaleziona transformacja pod kątem ilości punktów przystających.
		
	\section{Dyskusja na temat parametrów algorytmu}
		
		Algorytm ma wiele parametrów, od ustawienia których może zależeć jakość znalezionej transformacji, albo w ogóle powodzenie w jej znalezieniu. Pierwszym istotnym parametrem jest początkowa ilość iteracji. W wersji szeregowej pełni ona rolę maksymalnego czasowego, ograniczenia górnego wykonywania programu, zaś w wersji równoległej wprost wpływa na czas wykonywania (nie jest możliwe zatrzymanie algorytmu w czasie wykonywania i-tej iteracji). Tak jak już wcześniej wspomniano wybrano za optymalną wartość szesnastu bloków obliczeniowych po 1024 wątki, czyli $N=16\cdot1024=16384$ hipotezy.
		
		Drugim bardzo istotnym parametrem jest próg braku podobieństwa przy zastosowaniu testu trójkąta. Tak samo jak poprzedni parametr, wpływa on bezpośrednio na czas wykonywania algorytmu i został przyjęty jako wartość 0.2, czyli dozwala się $20\%$ różnicy między odpowiednimi bokami trójkąta przy estymacji transformacji. Taka wartość pozwala na odrzucenie ponad $99\%$ transformacji przy zachowaniu możliwości znalezienia poprawnej hipotezy, co zostało dokładnie przeanalizowane w następnym rozdziale.
		
		Ważnym elementem jest promień kuli, którą uważa się za sąsiedztwo transformowanego punktu. Jeżeli przyjmie się za duży, to estymowana transformacja może być niedokładna, jeżeli za mały to RANSAC nigdy nie znajdzie poprawnej transformacji. W obecnej implementacji przyjęto promień równy półtora wartości filtru wejściowego chmury, czyli $7.5~mm$ (chmura w początkowym etapie pracy programu jest przerzedzana według filtru wokselowego o boku $5~mm$).
		
		Ostatnim parametrem wymagającym dyskusji jest sam próg punktów przystających. Ciężko jest dobrać uniwersalną wartość sprawdzającą się przy każdym modelu i chmurze docelowej, dlatego możliwe, że programista będzie musiał świadomie dobierać tą wartość w zależności od rejestrowanego zestawu. W obecnej implementacji jest dobrana wartość odpowiadająca $80\%$ punktów chmury docelowej, co powinno zapewnić wzlględnie dobre dopasowanie modelu do obserwowanego obiektu.