\chapter{Algorytm RANSAC}
\label{sec::ransac_szeregowy}

	\section{Podstawowa wersja}
	
		Algorytm RANSAC (ang. Random Sample Consensus) to iteracyjna metoda estymacji parametrów modelu matematycznego na podstawie zbioru obserwacji, który może zawierać dużą liczba błędów grubych (ang. outliers). Należy on do dziedziny metod niedeterministycznych, co wiąże się z otrzymaniem poprawnego wyniku z określonym prawdopodobieństwem, które wzrasta wraz z liczbą przeprowadzonych iteracji. Algorytm jest wykorzystywany w różnych dziedzinach sztucznej inteligencji, jednak niniejsza praca skupia się na jego zastosowaniu w zagadnieniu rejestracji chmur punktów.
		
		\begin{figure}[h]
			\centering
			\begin{tikzpicture}[->,>=stealth']
				\tikzset{
					io/.style={
						circle,
						draw=black, very thick,
						minimum height=0.5cm,
						minimum width=1cm,
						text centered,
						font = \tiny,
					},
					op/.style={
						rectangle,
						rounded corners,
						draw=black, very thick,
						minimum height=1.2cm,
						text centered,
						text width=3.5cm,
						font = \tiny,
					},
					con/.style={
						rectangle,
						rotate=45,
						draw=black, very thick,
						minimum height=1.5cm,
						minimum width=1.5cm,
						font = \tiny,
					},
				}
			
			%START=======================================
			\node [io] (START) {\tiny START};
			
			%LOSOWANIE===================================
			\node [
				op,
				node distance=0cm,
				right of=START,
				yshift=-2cm,
			] (LOSOWANIE) {Losowy wybór trzech punktów chmury źródłowej};
		
			%ZNALEZIENIE=================================
			\node [
				op,
				node distance=0cm,
				right of=LOSOWANIE,
				yshift=-1.5cm
			] (ZNALEZIENIE) {Znalezienie punktów chmury docelowej o podobnych cechach};
		
			%OSZACOWANIE=================================
			\node [
				op,
				node distance=0cm,
				right of=ZNALEZIENIE,
				yshift=-1.5cm
			] (OSZACOWANIE) {Oszacowanie macierzy transformacji na podstawie 3 par punktów};
		
			%TRANSFORMACJA===============================
			\node [
				op,
				node distance=0cm,
				right of=OSZACOWANIE,
				yshift=-1.5cm
			] (TRANSFORMACJA) {Transformacja chmury źródłowej według macierzy transformacji};
		
			%OBLICZENIE==================================
			\node [
				op,
				node distance=0cm,
				right of=TRANSFORMACJA,
				yshift=-1.5cm
			] (OBLICZENIE) {Obliczenie liczby punktów pasujących do modelu (\emph{inliers})};
		
			%WARUNEK=====================================
			\node [
				con,
				node distance=0cm,
				right of=OBLICZENIE,
				shift = {(-1.5cm, -1.5cm)}
			] (WARUNEK) {\rotatebox{-45}{inliers $>$ próg}};
			
			\node [right of=WARUNEK, shift={(-2.25cm,0.25cm)}] (N) {\tiny NIE};
			\node [right of=WARUNEK, shift={(-0.5cm,-1.25cm)}] (T) {\tiny TAK};
			
			%STOP========================================
			\node [
				io,
				node distance=0cm,
				right of=WARUNEK,
				shift = {(0cm,-2cm)}
			] (STOP) {STOP};
			
			%PIERWSZY STOPIEN=============================
			\draw[->] (START) to (LOSOWANIE);
			\draw[->] (LOSOWANIE) to (ZNALEZIENIE);
			\draw[->] (ZNALEZIENIE) to (OSZACOWANIE);
			\draw[->] (OSZACOWANIE) to (TRANSFORMACJA);
			\draw[->] (TRANSFORMACJA) to (OBLICZENIE);
			\draw[->] (OBLICZENIE) to (WARUNEK);
			\draw[->] (WARUNEK.north west) to [out=-180,in=-90] +(-1.5cm,1.5cm) to +(0cm,7.2cm) to [out=90,in=90] (LOSOWANIE);
			\draw[->] (WARUNEK) to (STOP);
			
			\end{tikzpicture}
			\caption{Schemat blokowy podstawowej wersji algorytmu RANSAC stosowanej w dopasowywaniu chmur punktów}
			\label{fig::ransac_basic}
		\end{figure}
		
		Pierwszym krokiem każdej iteracji o numerze $k < N$ (N to maksymalna liszba iteracji) jest losowy wybór trzech punktów chmury źródłowej, czyli $\mgrSk = \{ \mgrPk{1}, \mgrPk{2}, \mgrPk{3} \}; ~ \mgrPk{1}, \mgrPk{2}, \mgrPk{3}~\in~S$, gdzie $S$ to zbiór punktów chmury źródłowej. Liczebność zbioru $\mgrSk$, czyli trzy punkty w przestrzeni $R^{3}$ to minimalna liczba punktów potrzebna, aby jednoznacznie opisać położenie danego obiektu w przestrzeni trójwymiarowej.
		
		Drugi etap polega na poddaniu zbioru $\mgrSk$ działaniu funkcji:
		\begin{equation}
			f : \mgrSk \rightarrow \mgrTk,
		\end{equation}
		gdzie zbiór $\mgrTk$ jest zdefiniowany jako podzbiór chmury docelowej: $\mgrTk = \{ \mgrQk{1}, \mgrQk{2}, \mgrQk{3} \};$ $ \mgrQk{1}, \mgrQk{2}, \mgrQk{3} \in T$. Funkcja $f$ wyznacza najbliższego sąsiada danego punktu chmury źródłowej w chmurze docelowej w wybranej przestrzeni cech lokalnych. Zarówno rodzaj wybranych cech lokalnych oraz sama funkcja $f$ będzie opisana bliżej w dalszej części pracy. W idealnym przypadku zbiór $\mgrTk$ jest zbiorem tych samych punktów co punkty zbioru $\mgrSk$, tylko ich akwizycja została dokonana z innego widoku. \pagebreak	
		
		Trzeci etap polega na oszacowaniu macierzy transformacji $\mgrZk$. Sam problem można opisać matematycznie jako:
		\begin{equation}
			\mgrQk{i} = \mgrRk ~ \mgrPk{i} + \mgrUk + \mgrVk,
		\end{equation}
		gdzie $\mgrRk$ to macierz rotacji o wymiarze $3 \times 3$, $\mgrUk$ to wektor translacji w przestrzeni 3D, a $\mgrVk$ to wektor szumów i wszystkie trzy parametry są oczywiście znajdowane dla konkretnej iteracji $k$ algorytmu. Rozwiązaniem tego równania jest szukana macierz transformacji:
		\begin{equation}
			\mgrZk = \left[ \begin{array}{cc} \mgrRkDash & \mgrUkDash \\ 0 & 1 \end{array} \right],
		\end{equation}
		którą otrzymuje się zazwyczaj za pomocą metody najmniejszych kwadratów, czyli minimalizacji wyrażenia
		\begin{equation}
			{\sum}^{2}_{k} = \sum_{i=1}^{3} || \mgrQk{i} - \mgrRkDash \mgrPk{i} - \mgrUkDash ||^2.
		\end{equation}
		Rozwiązanie tego równania zaczyna się od sprowadzenia punktów do środka układu współrzędnych poprzez:
		\begin{equation}
			p_{ci}^{k} = p_{i}^{k} - \overline{p}^{k}, ~ q_{ci}^{k} = q_{i}^{k} - \overline{q}^{k}.
		\end{equation}
		Następnie buduje się macierz kowariancji według:
		\begin{equation}
			\textbf{H}^{k} = \sum_{i=1}^{3} p_{ci}^{k}\cdot {q_{ci}^{k}}^{T},
		\end{equation}
		po czym dokonuje się rozkładu według wartości osobliwych macierzy H (ang. Singular Value Decomposition) \cite{estymacja}:
		\begin{equation}
			\textbf{H}^{k} = \textbf{W}^{k} ~ \boldmath{\Sigma}^{k} ~ {\textbf{V}^{k}}^{T}.
		\end{equation}
		Jest to znany i dobrze opisany problem algebry liniowej polegający na dekompozycji macierzy na iloczyn trzech specyficznych macierzy, z których $\textbf{W}^{k}$ oraz ${\textbf{V}^{k}}^{T}$ to macierze ortonormalne, zaś ${\boldmath{\Sigma}}^{k}$ to macierz diagonalna zawierająca uporządkowane nierosnąco, nieujemne wartości szczególne macierzy $\textbf{H}^{k}$. Dowiedziono, że szukana macierz rotacji jest opisana poprzez:
		\begin{equation}
			\mgrRkDash = \textbf{V}^{k} \cdot {\textbf{W}^{k}}^{T}.
		\end{equation}
		Teraz można łatwo obliczyć wektor translacji według:
		\begin{equation}
			\mgrUkDash = q_{ci}^{k} - \mgrRkDash p_{ci}^{k},
		\end{equation}
		co kończy ten etap algorytmu. Czwarty krok obejmuje transformację chmury źródłowej wedle estymowanej macierzy transformacji:
		\begin{equation}
			\mgrPk{j} = \left[ \begin{array}{c} \mgrP{j} \\ 1 \end{array} \right] \mgrZk = \left[ \begin{array}{c} \mgrP{j} \\ 1 \end{array} \right] \left[ \begin{array}{cc} \mgrRkDash & \mgrUkDash \\ 0 & 1 \end{array} \right],
		\end{equation}
		gdzie $j = 1, 2, ~ \dots, ~ |S|$, a zbiór wszystkich przetransformowanych punktów $\mgrPk{j}$ nazywany będzie zbiorem $S^{Z^{k}}$.
		
		W piątym kroku obliczana jest jakość estymacji macierzy transformacji $\eta$ i jest ona zdefiniowana jako:
		\begin{equation}
			\eta = \frac{|S^{Z^{k}}_{i}|}{|S|},
		\end{equation}
		gdzie $S^{Z^{k}}_{i}$ to zbiór punktów przetransformowanej chmury źródłowej spełniających poniższą zależność (ang. inliers):
		\begin{equation}
			\min_{j} \left( d(\mgrPk{},q_{j}) \right) < \delta,
			\label{eq::inliers}
		\end{equation}
		gdzie $\delta$ to ustalony z góry parametr. Oznacza to, że punkt przetransformowanej chmury źródłowej jest uznawany za poprawnie dopasowany, jeżeli ma w swoim otoczeniu o promieniu $\delta$ jakikolwiek punkt chmury docelowej. Jako otoczenie przyjmuje się zazwyczaj półtorej wartości średniej częstości występowania punktów. Algorytm kończy się, jeżeli zrealizowane zostały wszystkie założone na początku iteracje lub jeżeli jakość dopasowania $\eta$ jest większa niż założony wcześniej próg $\eta_{th}$.
		
		Kończąc rozważania dotyczące standardowej wersji algorytmu RANSAC należy powiedzieć kilka słów o jego parametrach. Załóżmy, że prawdopodobieństwo znalezienia pasującej transformacji to $P_{S}$ oraz prawdopodobieństwo, że żaden z trzech wylosowanych w kroku pierwszym punktów nie jest błędem grubym to $1-{P_{W}}^{3}$, wtedy według \cite{dummies} zachodzić będzie zależność:
		\begin{equation}
			(1-P_{S}) = (1-{P_{W}}^{3})^{N},
		\end{equation}
		gdzie N to wymagana liczba iteracji. Problemem jest fakt, że oba zarówno parametr $P_{S}$, jak i $P_{W}$ nie są znane \textit{a priori} i ich dobór odbywa się w sposób empiryczny: $P_{S}$ przyjmuje się jako wartość z przedziału $0.95 - 0.99$, zaś $P_{W}$ szacuje się jako stosunek mocy zbioru punktów właściwych chmury źródłowej (bez błędów grubych) do mocy zbioru wszystkich punktów chmury źródłowej $S$. Wtedy liczba iteracji algorytmu szacuje się według:
		\begin{equation}
			N = \frac{\log (1-P_{S})}{\log (1-{P_{W}}^{3})}.
		\end{equation}
		
	\section{O cechach i sąsiadach}
	\label{sec::neighbors_serial}
	
		Elementem wymagającym doprecyzowania jest wybór punktu odpowiadającego w drugim kroku algorytmu RANSAC. Dla ludzi stwierdzenie, że jeden obiekt jest podobny do innego jest naturalne, automatyczne, jednak algorytm musi mieć jakiś deskryptor opisujący w sposób ilościowy dany punkt przestrzeni pod jakimś ściśle określonym kątem. Do tego wykorzystuje się właśnie przestrzenie cech lokalnych. Najczęściej używanymi deskryptorami w rejestracji chmur punktów są cechy z rodziny SHOT (ang. Unique Signatures of Histograms) \cite{shot} oraz z rodziny PFH (ang. Point Feature Histograms) \cite{pfh}.
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=0.4\linewidth]{img/shot.png}
			\caption { Algorytm liczenia cechy SHOT dla jednego punktu chmury \cite{comparison_of_features}.}
			\label{fig::shot}
		\end{figure}
		
		Cechy SHOT wprowadzają dla każdego punktu lokalny układ współrzędnych jako wektory własne macierzy kowariancji obliczonej na podstawie najbliższych sąsiadów każdego punktu. Wyznaczony układ współrzędnych dzieli przestrzeń na siatkę w układzie sferycznym, a następnie dla każdej komórki tworzony jest histogram kątów zawartych między normalną punktu centralnego, a punktów sąsiadujących -- algorytm liczenia cechy dla jednego punktu chmury został pokazany na rysunku \ref{fig::shot}. Jeżeli przestrzeń została podzielona na $k$ komórek i w każdej komórce jest $b$ komórek histogramu, to finalnie cechę SHOT dla każdego punktu chmury opisuje $k \cdot b$ liczb zmiennoprzecinkowych.
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=0.4\linewidth]{img/pfh.png}
			\caption { Algorytm liczenia cechy PFH dla jednego punktu chmury \cite{comparison_of_features}.}
			\label{fig::pfh}
		\end{figure}
		
		PFH  oprócz samej użyteczności w jako deskryptor dla rejestracji chmur punktów jest też używany do klasyfikacji samych punktów jako krawędzie, narożniki, czy płaszczyzny. Jej liczenie polega na konstrukcji ramki Darboux \cite{darboux} między wszystkimi parami punktów wewnątrz otoczenia danego punktu. Trzy kąty oraz odległość opisujące zależność między każdą parą punktów i ich normalnych są zbierane w postaci histogramu -- algorytm liczenia cechy dla jednego punktu chmury został pokazany na rysunku \ref{fig::pfh}. Liczenie cech PFH zajmuje dużo czasu, dlatego szybko po opublikowaniu pracy powstał kolejny artykuł proponujący uproszczenie wcześniejszej ideii do cech FPFH (ang. Fast Point Feature Histograms) \cite{fpfh}.
		
		Wariant FPFH zwiększa szybkość obliczeń kosztem ich dokładności. Odrzucona została odległość między punktami, zaś pary tworzone są tylko przy wykorzystaniu punktu, dla którego liczona jest cecha. Przez to zamiast $b^4$ komórek, gdzie b to rozmiar histogramu na jeden wymiar użytych jest $3 \cdot b$ komórek. 
		
		Według \cite{comparison_of_features} najlepszy stosunek czasu liczenia do jakości danej cechy na potrzeby rejestracji chmur punktów reprezentuje deskrytpor FPFH. Przemawia za tym oprócz wyższego prawdopodobieństwa znalezienia korespondencji mniejszy wymiar wektora cech -- w przypadku użycia cech SHOT generuje się histogram złożony z $11$ komórek dzieląc przestrzeń na $32$ obszary, co daje $352$ liczby zmiennoprzecinkowe dla każdego punktu, gdzie w przypadku użycia cech FPFH wystarczy sam 11-komórkowy histogram. Cechy FPFH zajmują mniej miejsca w pamięci i szybciej się je liczy, co jest bardzo ważnym atutem w przypadku zastosowań czasu rzeczywistego.
		
		Oprócz samego deskryptora warto również omówić procedurę szukania najbliższego sąsiada w przestrzeni cech. Naiwne przeszukiwanie całej chmury docelowej w poszukiwaniu punktu najbardziej podobnego byłoby bardzo czasochłonne, dlatego stosuje się szybkie metody przeszukiwania oparte na mechanizmie drzew binarnych. Obecnie najbardziej efektywną metodą przeszukiwania jest użycie drzewa kd \cite{kdtree}, które polega na dzieleniu zbioru na część lewą i prawą, gdzie czynnikiem dzielącym jest mediana wartości w danym wymiarze. Jak raz zbuduje się takie drzewo to przechodzenie po nim i szukanie najbliższego sąsiada w 33-wymiarowej przestrzeni jest szybkie i efektywne.
	
	\section{Zalety i wady}
	
		Algorytm RANSAC ma szereg zalet wynikających z jego heurystycznej i niedeterministycznej natury. Jest on w stanie wygenerować akceptowalne rozwiązanie nawet z dużą liczbą błędów grubych, co w przypadku rejestracji chmur punktów oznacza większą tolerancję błędów sensora, obserwowanej powierzchni, okluzji itp. Błędy grube pojawiają się przykładowo przy niepoprawnej segmentacji (część innego elementu sceny jest uznana za obiekt do dopasowania) lub mogą oznaczać brak dużej części obiektu, który jest przysłonięty przez inny. RANSAC dzięki swojej stochastycznej naturze jest w stanie znaleźć poprawne dopasowanie między takim zbiorem danych wejściowych.
		
		Bardzo ważną zaletą jest brak konieczności znajomości początkowego położenia i orientacji rejestrowanych chmur punktów, bez której inne algorytmy (np. ICP) nie byłyby w stanie działać. Zmniejszenie liczby warunków początkowych zwiększa jego wachlarz możliwych aplikacji, co w połączeniu z łatwością implementacji argumentuje, dlaczego jest to jeden z najczęściej wykorzystywanych algorytmów w rejestracji chmur punktów.
		
		Prostota algorytmu okupiona jest brakiem gwarancji znalezienia poprawnego rozwiązania oraz dość dużym czasem wykonywania algorytmu. Krytycznym punktem jest potrzeba transformacji całej chmury źródłowej, która może zawierać nawet kilkaset tysięcy punktów, w celu weryfikacji poprawności hipotezy (transformacji). Samo sprawdzenie jakości dopasowania również jest czasochłonne, ponieważ dla każdego punktu przetransformowanej chmury źródłowej zachodzi konieczność znalezienia najbliższego sąsiada wśród całej chmury docelowej. Przy możliwościach dzisiejszego sprzętu komputerowego nie jest to duży rząd wielkości, jednak mnożąc ten czas przez liczbę iteracji (która czasami sięga kilku tysięcy) otrzymuje się czas działania algorytmu rzędu kilku, kilkunastu albo nawet kilkudziesięciu sekund.
		
	\section{Rozszerzone wersje}
	\label{sec::extended}
	
		Już na pierwszy rzut oka widać, że algorytm ma wiele punktów gotowych na potencjalne modyfikacje i faktycznie takich modyfikacji jest wiele. Według \cite{comparative} można rozróżnić dwa punkty skupiające możliwe ulepszenia algorytmu: niejednorodny wybór punktów w fazie generacji hipotezy oraz optymalizacja procesu jej weryfikacji. W niniejszej pracy krótko omówiony zostanie jeden przedstawiciel pierwszej grupy (Lo-RANSAC) oraz cztery przedstawiciele drugiej grupy modyfikacji (rejekcja poly, test $T_{d,d}$, test \textit{"bail-out"} oraz test widoczności).
		
		\subsection{Lo-RANSAC}
		
			Jednym z założeń terminacji iteracji podstawowej wersji algorytmu jest spójność wyznaczonego modelu ze wszystkimi punktami przystającymi, jednak to nie jest zawsze prawdą, ponieważ dane sensoryczne są zaszumione. Z tego względu zaproponowany został lokalnie zoptymalizowana wersja algorytmu \cite{local_ransac}, w której stała liczba hipotez ($\sim 20$) jest generowana używając punktów przystających do najlepszej dotychczas hipotezy. Jak dowodzą autorzy, takie podejście powoduje szybsze osiągnięcie kryterium zakończenia algorytmu.

		\subsection{Test trójkąta}
		\label{sec::poly}
		
			Jest to metoda wstępnego odrzucenia hipotezy jeszcze przed estymacją transformacji przedstawiona w \cite{poly}. Opiera się ona na porównaniu wielokątów rozpiętych na punktach chmury źródłowej oraz docelowej -- jeżeli stosunek odpowiednich boków tego trójkąta nie jest zachwiany, to hipoteza jest odrzucana. Takie podejście bazuje na założeniu, że dopasowywane są obiekty o charakterze bryły sztywnej, w której odległości między odpowiednimi punktami nie zmieniają się w czasie. Matematycznie opisuje się boki trójkąta rozpartego na punktach chmury źródłowej jako:
			\begin{equation}
				d_{p_{i}} = ||p_{(i+1~mod~3)} - p_{i}||.
			\end{equation}
			Tak samo opisywany jest trójkąt rozparty na punktach chmury docelowej, po czym definiuje się wektor relatywnej różnicy boków według:
			\begin{equation}
				\delta_{i} = \left[ \frac{|d_{p_{i}} - d_{q_{i}|}}{max(d_{p_{i}},d_{q_{i})}} \right].
			\end{equation}
			Punkty uznawane są za zdolne do wygenerowania poprawnej hipotezy, jeśli każdy z elementów powyżej zdefiniowanego wektora jest mniejszy od założonego progu. Takie wstępne odrzucenie jest bardzo wartościowe z punktu widzenia szybkości działania algorytmu szeregowego.
		
		\subsection{Test $T_{d,d}$}
		\label{sec::testtdd}
		
			Zamiast kroku czwartego standardowej wersji algorytmu RANSAC, czyli transformacji całej źródłowej chmury punktów w pracy \cite{tdd} zaproponowany został wstępny test wygenerowanej hipotezy. Weryfikacja modelu przebiega najpierw na podzbiorze $d$ losowo wybranych punktów (gdzie $d \ll |S|$). Jakość dopasowania pozostałych $|S| - d$ punktów jest sprawdzana dopiero, jeżeli wszystkie $d$ punktów są punktami przystającymi, czyli należą do zbioru $S_{i}$ i spełniają zależność (\ref{eq::inliers}), gdzie proponowana przez autorów wartość to $d=1$. Dzięki takiemu podejściu prawdopodobieństwo, że poprawna hipoteza zostanie odrzucona jest stosunkowo małe, a zysk czasowy z braku konieczności transformowania całej chmury punktów i jej weryfikacji czyni tę modyfikację wartościową.
		
		\subsection{Test \textit{"bail-out"}}
		
			Jest to rozszerzenie testu $T_{d,d}$ zaproponowane w \cite{bailout} i również polega na wstępnym odrzuceniu hipotezy przed transformacją całej chmury punktów. Mając dany podzbiór $n$ punktów zakłada się, że liczba punktów przystających $\kappa_n$ w tym podzbiorze jest opisana rozkładem hipergeometrycznym: $\kappa_{n} \sim HypG(\kappa,n,\overline{\kappa},N)$, gdzie $\overline{\kappa}$ to całkowita liczba punktów przystających dla aktualnej hipotezy, $N$ to całkowita liczba punktów chmury docelowej. Następnie mając najlepszą dotychczasową hipotezę wraz z całkowitą liczbą punktów przystających w tej hipotezie jako $\overline{\kappa}_{best}$ należy rozważyć jakość dopasowania aktualnej hipotezy wraz z dotychczasową liczbą punktów przystających $\kappa$. Jako że takie rozumowanie jest złożone obliczeniowo można to na poziomie ufności $P_{conf}$ uprościć do określenia minimalnego progu $\kappa^{n}_{min}$ poniżej którego dana hipoteza będzie odrzucana (będzie następował \textit{bail-out}). Jak widać test $T_{d,d}$ jest specjalnym przypadkiem opisanego tutaj testu.
			
		\subsection{Test widoczności}
		
			W cytowanym już w tej pracy artykule \cite{nieznane} pokazano inną metodę wstępnego odrzucenia hipotezy opartą na rozróżnieniu obszarów widocznych i zasłoniętych. Przy założeniu, że chmura punktów jest dostarczona wprost przez sensor głębi (np. Microsoft Kinect) znana jest informacja, w którym miejscu znajduje się sensor -- które obszary są widoczne (puste), a które zasłonięte przez jakiś obiekt występujący na scenie. W niniejszej pracy zauważono, że przetransformowane punkty znajdujące się w obszarze niewidocznym nie można jednoznacznie zaklasyfikować jako punkty przystające lub odstające. W zaproponowanej wersji algorytmu RANSAC była przedstawiona fuzja testu $T_{d,d}$ z wiedzą o obszarach nieznanych i hipoteza odrzucana była, jeżeli przynajmniej dwa z dziesięciu testowanych punktów znajdowały się w obszarze, który powinien być pusty (obszar przed sceną).
		
	\section{Możliwości zrównoleglenia}
	
		Jak zostało przedstawione w rozdziale \ref{sec::programowanie_rownolegle}, aby móc efektywnie zrównoleglić dany algorytm, musi on być rozkładalny na dużą liczbę mniejszych podproblemów, które są dodatkowo niezależne od siebie. Wiele algorytmów iteracyjnych mogłoby mieć trudności w równoległej wersji, gdyż czasami każda iteracja zależna jest od wcześniejszego wyniku działania pętli. RANSAC jednak nie ma takiego problemu, gdyż każda pętla $k$ liczona jest niezależnie, przez co pod tym względem wydaje się dobrze nadawać do realizacji równoległej.
		
		Kolejnym aspektem jest długość ścieżki krytycznej -- tak jak zostało to omówione w rozdziale~\ref{sec::programowanie_rownolegle}. Należy zauważyć, że każdy krok pokazany na rysunku \ref{fig::ransac_basic} reprezentuje czynność, którą należy realizować szeregowo, jednak zadania wewnątrz każdego kroku mogą być realizowane w sposób masowo--równoległy.
		
		Najważniejszym argumentem jest potencjalny zysk czasowy, jaki można odnieść przy realizacji takiego algorytmu. Jeżeli system zamiast kilkudziesięciu sekund byłby w stanie dać wynik w czasie poniżej sekundy, to roboty mobilne mogłyby być wykorzystywane do rzeczywistych zadań manipulacyjnych.