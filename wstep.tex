\chapter{Wstęp}

	W roku obecnym przypada pięćdziesiąta rocznica sformułowania przez Gordona Moore'a empirycznego prawa rozwoju elektroniki krzemowej \cite{moore}. Współzałożyciel korporacji Intel zaobserwował, że ekonomicznie uzasadniona liczba tranzystorów w układzie scalonym zwiększa się  zgodnie z trendem wykładniczym, podwajając się w niemal równych odstępach czasu. Co prawda obecnie producenci układów scalonych zbliżają się do fizycznej granicy możliwości miniaturyzacji podyktowanej skończonymi rozmiarami atomów oraz górną granicą prędkości przesyłania informacji (prędkość światła), jednak prawo to można odnieść do wielu innych parametrów sprzętu komputerowego. Dobrym przykładem owego ciągłego rozwoju jest fakt, że komputery statków kosmicznych misji programu Apollo miały słabsze procesory od większości współczesnych smartfonów \cite{nasa}.
	
	\begin{figure}[h]
		\begin{minipage}[b]{0.425\linewidth}
			a) \\
			\includegraphics[width=\linewidth]{img/gordon_1.png}
		\end{minipage}
		\quad
		\begin{minipage}[b]{0.475\linewidth}
			b) \\
			\includegraphics[width=1.12\linewidth]{img/gordon_2.jpg}
		\end{minipage}
		\caption {a) Przewidywany przez G. Moore'a w 1965 roku wykres liczby tranzystorów w jedym układzie scalonym \cite{moore}; b) Wykres rzeczywisty wraz z naniesioną ceną za jeden tranzystor \cite{moore_2}.}
		\label{fig::moore}
	\end{figure}
	
	\pagebreak
	
	Postęp technologiczny otwiera nowe horyzonty dla rozwoju takich dziedzin jak automatyka, robotyka, czy sztuczna inteligencja, jednak wizje maszyn równie sprawnych i inteligentnych jak ludzie pozostają jeszcze w kategoriach fantastyki naukowej. Faktycznie znane są już w literaturze zaawansowane metody sztucznej inteligencji do uczenia maszynowego, podejmowania decyzji czy  logicznego/racjonalnego rozumowania. Autor niniejszej pracy jest jednak przekonany, że powyższe zagadnienia nie mogą poprawnie działać bez odpowiedniego rozumienia otoczenia, czyli szeroko rozumianej percepcji.
	
	Jednym z podstawowych problemów percepcji robotów jest umiejętność ciągłego dopasowania chmur punktów uzyskanych w kolejnych klatkach w celu uzyskania pełnego modelu 3D otoczenia. Zagadnienie to jest nazywane rejestracją. Wynikiem takiego algorytmu jest macierz transformacji każdego z widoków do wspólnego globalnego układu odniesienia. Mając jeden, większy i dokładniejszy model otoczenia robota mobilnego można o wiele więcej się dowiedzieć niż przy próbie analizy każdej klatki z osobna \cite{kinfu}.
	
	Na rysunku (\ref{fig::pcl_1}) pokazano przykład sześciu chmur punktów będących różnym wycinkiem jednego pomieszczenia i zebranych za pomocą skanera laserowego 2D umieszczonego na obrotowej głowicy. Po dopasowaniu ich do wspólnego modelu (rysunek \ref{fig::pcl_2}) dalsze zadanie rozpoznawania obiektów w otoczeniu robota jest zdecydowanie ułatwione.
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{img/pcl_1.jpg}
		\caption { Sześć widoków jednego pomieszczenia zebranych z różnej perspektywy \cite{rusu}.}
		\label{fig::pcl_1}
	\end{figure}
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.8\linewidth]{img/pcl_2.jpg}
		\caption {Wynik rejestracji chmur punktów pokazanych na rysunku \ref{fig::pcl_1} w jedną modelową chmurę punktów \cite{rusu}.}
		\label{fig::pcl_2}
	\end{figure}
	
	Rejestracją nazywa się również dopasowanie modelu do konkretnego obiektu na scenie. Sama detekcja obiektu w otoczeniu robota nadaje semantyczne znaczenie tej części przestrzeni, jest jednak niewystarczająca do zadań manipulacji tym elementem. Dopasowanie modelu do jego fizycznego odpowiednika w otoczeniu robota umieszcza lokalny układ współrzędnych związany z tym obiektem, umożliwiając jego manipulację. Na rysunku (\ref{fig::model}) pokazano wynik takiego dopasowania modelowej chmury punktów do obserwowanej sceny \cite{nieznane}.
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.6\linewidth]{img/model.png}
		\caption {Przykład dopasowania modelu drukarki do obserwowanej sceny \cite{nieznane}.}
		\label{fig::model}
	\end{figure}
	
	Przedstawione powyżej dwa rozumienia zagadnienia rejestracji sprowadzają się do tego samego zadania: znalezienia dopasowania takich samych elementów obecnych w dwóch różnych reprezentacjach mających swoje własne układy współrzędnych. Problem ten staje się łatwo rozwiązywalny, jeżeli znane są współrzędne odpowiadających sobie punktów chmury źródłowej i docelowej. W rzeczywistych warunkach nie jest jednak spełnione to założenie, ponieważ każdy pomiar jest obarczony szeregiem błędów uniemożliwiających znalezienie dokładnych odpowiedników punktowych i jest to powód, dla którego używane są metody przybliżone.
	
	Jedną z najbardziej popularnych metod rejestracji niezorganizowanej chmury punktów jest algorytm ICP (and. Iterative Closest Point) \cite{icp}, który stara się znaleźć optymalną transformację za pomocą minimalizacji błędu odległości między dopasowanymi chmurami. Jak sama nazwa wskazuje, jest to iteracyjny algorytm, którego każdy krok składa się z kilku etapów. W pierwszym wyszukiwane są pary najbliższych punktów chmury źródłowej oraz docelowej, przy czym uwzględnia się, że nie wszystkie punkty mogą mieć najbliższych sąsiadów w drugim zbiorze poprzez przypisanie odpowiedniej wagi do danego składnika sumy. Następnie wyliczany jest błąd dopasowania i transformacja, której celem jest zminimalizowanie obliczonego błędu.
	
	Algorytm ICP ma wiele ciekawych modyfikacji, z czego większość to próba realizacji algorytmu nieliniowego lub uwzględniającego zmienność samego obiektu \cite{icp_variants}. Większość z nich ma jednak wspólne wady takie jak skłonność do wpadania w minima lokalne błędu dopasowania, na ogół dość duża liczba iteracji, czy potrzeba wstępnego, zgrubnego dopasowania dwóch widoków.
	
	Zarówno przy zgrubnym dopasowaniu chmur punktów, jak i ostatnim etapie detekcji obiektów na scenie używa się zaproponowanego w latach osiemdziesiątych XX wieku heurystycznego algorytmu RANSAC. Jest to szeroko używany algorytm, który po ponad trzydziestu latach od pierwszego sformułowania doczekał się niezliczonej ilości artykułów z propozycjami ulepszeń, czy z opisem zastosowań w różnych dziedzinach nauki. W ostatnich latach daje się zauważyć rosnące apetyty naukowców do użycia go w aplikacjach działających w warunkach rzeczywistych (ang. real-time). Jest to o tyle utrudnione, że chmury zawierają czasem setki tysięcy punktów, przez co rejestracja takich dużych obiektów zajmuje często kilkanaście lub nawet kilkadziesiąt sekund.
	
	Istnieje kilka podejść do dostosowania algorytmu na potrzeby zadań w warunkach rzeczywistych, czego przykładem jest próba narzucenia stałego budżetu czasowego w pracy \cite{comparative}. Algorytm ma za zadanie znaleźć najlepsze rozwiązanie w danym przedziale czasowym, jednak jest to pewnego rodzaju kompromis. Jako alternatywę do takich działań można rozważyć użycie technologii programowania równoległego.
	
	Projekt jest pewnego rodzaju rozszerzeniem pracy zaprezentowanej w \cite{nieznane}, która była pierwszą próbą usprawnienia procesu rejestracji chmur punktów. Praca została dobrze przyjęta przez towarzystwo naukowe Krajowej Konferencji Robotyki i zarekomendowana do dalszej publikacji w języku angielskim. Niniejsza praca zamiast wprowadzania modyfikacji metody skupia się na podejściu do zagadnienia od strony innego sposobu prowadzenia obliczeń. Celem pracy jest przyspieszenie działania algorytmu rejestracji chmur punktów, zaś zakres to zaprojektowanie oraz implementacji równoległego algorytmu RANSAC w technologi CUDA.
	
	\pagebreak
	
	Na początku pracy w rozdziale \ref{sec::programowanie_rownolegle} opisano ideę programowania równoległego wraz z trudnościami, jakie nastręcza programistom. Omówiono również pokrótce technologię CUDA, która pozwala na szybkie pisanie algorytmów równoległych oraz późniejszą optymalną pracę wykorzystując niskobudżetowe układy na kartach graficznych. Następnie opisano standardowy algorytm RANSAC wraz z najnowszymi wieściami z naukowego frontu jego ulepszeń (rozdział \ref{sec::ransac_szeregowy}). Przeanalizowano również jego wady i zalety oraz  możliwość realizacji równoległej wersji. Następnie w rozdziale \ref{sec::ransac_rownolegly} zaproponowano ideową wersję równoległego algorytmu RANSAC oraz jego implementację w technologii CUDA, którą następnie poddano testom w rozdziale \ref{sec::testy}, zaś ostateczną refleksję oraz plany na przyszłość przedstawiono w rozdziale \ref{sec::podsumowanie}. Jako dodatek \ref{appendix} załączono fragment kodu źródłowejgo w postaci jądra obliczeniowego CUDA pracującego nad znalezieniem najbliższych sąsiadów w przestrzeni cech lokalnych FPFH.