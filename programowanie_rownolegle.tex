\chapter{Programowanie równoległe}
\label{sec::programowanie_rownolegle}

	\section{Idea}
		
		Programowanie równolegle to filozofia prowadzenia obliczeń w taki sposób, że dany do rozwiązania problem dzielony jest na mniejsze podproblemy, które mogą być wykonane równocześnie w ramach dostępnych jednostek obliczeniowych. Taki rodzaj tworzenia programu w wielu przypadkach pozwala zwiększyć wydajność obliczeń i skrócić czas rozwiązania konkretnego pojedynczego zadania obliczeniowego.
		
		Należy zaznaczyć, że główną ideą przetwarzania równoległego jest podział postawionego problemu na podzadania, które mogą być rozwiązane w sposób niezależny od siebie. Stopień takiego podziału decyduje o tym, czy dany problem jest możliwy i opłacalny do zrównoleglenia, gdyż nakład pracy programisty przy tworzeniu programu równoległego jest znacznie większy niż w przypadku tradycyjnego programowania sekwencyjnego. Zwyczajowo uważa się, że migracja programu sekwencyjnego na równoległy jest opłacalna przy zmniejszeniu czasu jego wykonywania od kilkunastu do kilkudziesięciu razy.
		
		Zysk wydajności rozwiązania problemu jest kuszącą perspektywą, jednak występowanie niezależnych podzadań, które zazwyczaj muszą się ze sobą komunikować i współpracować generuje szereg niebezpieczeństw takich jak zależność obliczeniowa, czy problemy komunikacyjne między wątkami.
		
	\section{Zagrożenia}
		
		Pojęcie zależności obliczeń polega na konieczności podziału rozwiązania problemu na części, w których wejście danego modułu stanowią dane przetworzone przez inny moduł. W programowaniu tradycyjnym taki problem nie występuje, ponieważ algorytm z natury wykonywany jest szeregowo. W programowaniu równoległym należy podzielić program na niezależne od siebie fragmenty, czyli takie, w których kolejność wykonywania obliczeń nie wpływa na ostateczny wynik programu. Uszeregowane w odpowiedniej kolejności niezależne fragmenty programu nazywa się ścieżką krytyczną.
		
		Opis matematyczny zagadnienia zależności obliczeniowej definiują warunki Bernsteina \cite{bernstein}. Zakładając, że dane są dwa fragmenty programu: $P_{i}$ i $P_{j}$ wraz z odpowiednimi zbiorami danych wejściowych: $I_{i}$ i $I_{j}$ oraz danych wyjściowych: $O_{i}$ i $O_{j}$ to wspomniane fragmenty programu uznaje się za niezależne, jeżeli zachodzi warunek poniżej:
		\begin{equation}
			I_{j} \cap O_{i} = \emptyset ~ \wedge ~ I_{i} \cap O_{j} = \emptyset ~ \wedge ~ O_{i} \cap O_{j} = \emptyset.
		\end{equation}
		
		W przypadku, gdy pierwsze dwa człony powyższej zależności są nieprawdziwe, występować będzie tzw. zależność przepływu, czyli jeden fragment programu bazuje na wyniku działania innej jego części. Jeżeli zaś nieprawdziwy jest trzeci człon, to występować będzie niezależność wyjścia, co oznacza, że wynik działania jednej części programu będzie zapisywany w tym samym miejscu co innej jego części.
		
		Drugim sztandarowym przykładem problemów, jakie spotyka programista po migracji z tradycyjnego programowania na równoległe są problemy komunikacyjne. Występują one w przypadku, kiedy podzadania wymagają wymiany informacji między sobą, a najprostszym przykładem jest wstępujący w wielu dziedzinach techniki hazard (ang. race condition). W tym przypadku przejawia się on przez przeplot zapisu i odczytu jednej zmiennej przez dwa podprocesy.
		
		\begin{figure}[h]
			\centering
			\begin{tikzpicture}[->,>=stealth']
				\tikzset{
					op/.style={
						rectangle,
						rounded corners,
						draw=black, very thick,
						minimum height=1cm,
						text centered,
						text width=2.0cm,
						font = \tiny,
					},
				}
			
			%A1==========================================
			\node [op] (A1) {local = global};
			
			%A2==========================================
			\node [
				op,
				node distance=3cm,
				right of=A1,
				yshift=0cm,
			] (A2) {local = local+1};
			
			%A3==========================================
			\node [
				op,
				node distance=3cm,
				right of=A2,
				yshift=0cm,
			] (A3) {global = local};
			
			%B1==========================================
			\node [
				op,
				node distance=0.5cm,
				right of=A1,
				yshift=-1.5cm,
			] (B1) {local = global};
			
			%B2==========================================
			\node [
				op,
				node distance=3cm,
				right of=B1,
				yshift=0cm,
			] (B2) {local = local+1};
			
			%B3==========================================
			\node [
				op,
				node distance=3cm,
				right of=B2,
				yshift=0cm,
			] (B3) {global = local};
			
			
			\node [shift={(-3.25cm,-1cm)}, rotate=90] {\tiny Początek jądra obliczeniowego};
			\draw [-] (-3,-3) -- (-3,1); 
			
			\draw [->,green,snake=snake,
				   segment amplitude=.5mm,
				   segment length=2mm,
				   line after snake=0.5mm] (-3,0) -- (-1.15,0);
			\node [shift={(-2cm,+0.25cm)}] {\tiny Wątek 1};
				   
			\draw [->,green,snake=snake,
				   segment amplitude=.5mm,
				   segment length=2mm,
				   line after snake=0.5mm] (1.15,0) -- (1.85,0);
				   
			\draw [->,green,snake=snake,
				   segment amplitude=.5mm,
				   segment length=2mm,
				   line after snake=0.5mm] (4.15,0) -- (4.85,0);
				   
			\draw [->,green,snake=snake,
				   segment amplitude=.5mm,
				   segment length=2mm,
				   line after snake=0.5mm] (7.15,0) -- (8.5,0);
				   
			\draw [->,brown,snake=snake,
				   segment amplitude=.5mm,
				   segment length=2mm,
				   line after snake=0.5mm] (-3,-1.5) -- (-0.65,-1.5);
			\node [shift={(-2cm,-1.25cm)}] {\tiny Wątek 2};

			\draw [->,brown,snake=snake,
				   segment amplitude=.5mm,
				   segment length=2mm,
				   line after snake=0.5mm] (1.65,-1.5) -- (2.35,-1.5);
				   
			\draw [->,brown,snake=snake,
				   segment amplitude=.5mm,
				   segment length=2mm,
				   line after snake=0.5mm] (4.65,-1.5) -- (5.35,-1.5);
				   
			\draw [->,brown,snake=snake,
				   segment amplitude=.5mm,
				   segment length=2mm,
				   line after snake=0.5mm] (7.65,-1.5) -- (8.5,-1.5);
				   
			\node [shift={(8.75cm,-1cm)}, rotate=90] (K) {\tiny Koniec jądra obliczeniowego};
			\draw [-] (8.5,-3) -- (8.5,1);
			
			\draw [->] (-3,-2.75) -- (8.5,-2.75);
			\node [shift={(-2cm,-2.5cm)}] {\tiny Czas};
			
			\draw [->, dashed] (-1.1,-0.5) -- (-1.1,-2.75);
			\draw [->, dashed] (1.9,-0.5) -- (1.9,-2.75);
			\draw [->, dashed] (4.9,-0.5) -- (4.9,-2.75);
			
			\draw [->, dashed] (-0.6,-2) -- (-0.6,-2.75);
			\draw [->, dashed] (2.4,-2) -- (2.4,-2.75);
			\draw [->, dashed] (5.4,-2) -- (5.4,-2.75);
			
			\fill (-1.1,-2.75) circle[radius=1.5pt];
			\fill (1.9,-2.75) circle[radius=1.5pt];
			\fill (4.9,-2.75) circle[radius=1.5pt];
			
			\fill (-0.6,-2.75) circle[radius=1.5pt];
			\fill (2.4,-2.75) circle[radius=1.5pt];
			\fill (5.4,-2.75) circle[radius=1.5pt];
			
			\end{tikzpicture}
			\caption{Ideowy przykład problemu hazardu w programowaniu równoległym.}
			\label{fig::race_condition}
		\end{figure}
		
		Na rysunku \ref{fig::race_condition} przedstawiono dwa podprogramy, które mają za zadanie inkrementację zmiennej \textit{global} zapisanej w pamięci globalnej na urządzeniu (o hierarchii pamięci będzie w dalszej części dokumentu). Problem hazardu pojawia się w momencie dostępu do pamięci globalnej przez jeden podprogram, zanim drugi zapisze wynik swojego działania, dlatego istnieje tutaj potrzeba użycia mechanizmu synchronizacji.
		
		Przedstawione powyżej dwa zagrożenia pokazują, że programowanie równoległe ma duże możliwości przyspieszenia niektórych algorytmów, jednak programista musi o wiele ostrożniej projektować rozwiązanie problemu niż w przypadku tradycyjnego sekwencyjnego podejścia. Teoria obliczeń równoległych jest szeroką dziedziną i nie jest głównym tematem tej pracy. Więcej informacji można znaleźć w \cite{parallel_theory}.
		
	\section{Technologia Nvidia CUDA}
	
		 CUDA (ang. Compute Unified Device Architecture) to opracowana przez firmę Nvidia uniwersalna architektura procesorów wielordzeniowych (głównie kart graficznych) umożliwiająca wydajniejsze wykorzystanie ich mocy obliczeniowej niż w tradycyjnych procesorach ogólnego zastosowania \cite{cuda}. Dodatkowo oprócz standardu sprzętowego w skład technologii CUDA wchodzi zbiór bibliotek i narzędzi wysokiego poziomu umożliwiających łatwe korzystanie z owej architektury w celu realizacji różnych problemów w sposób masowo równoległy. Głównym nurtem w programowaniu układów o architekturze zgodnej z CUDA stanowi rozszerzenie języka C dostarczające programistom szereg instrukcji pozwalających na tworzenie kodu źródłowego wykonywanego później na karcie graficznej.
		 
		 Technologia pierwotnie była skierowana dla branży grafiki komputerowej (a szczególnie w grach komputerowych), w której zachodzi potrzeba coraz to bardziej wydajnego renderingu, przetwarzania obrazów czy wideo. Naukowcy bardzo szybko docenili możliwości obliczeniowe układów teksturujących i zaczęli je używać do swoich obliczeń i w ten sposób karty graficzne stały się narzędziami obliczeniowymi w dziedzinie kryptografii, sztucznej inteligencji, czy nawet symulacji ruchu ciał astronomicznych.
		
	\section{Architektura CPU kontra GPU}
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=\linewidth]{img/cpuvsgpu_1.png}
			\caption {Teoretyczna liczba GFLOP/s (operacji stało i zmiennoprzecinkowych na sekundę liczona w miliardach) współczesnych układów CPU oraz GPU \cite{cuda}.}
			\label{fig::cpuvsgpu_1}
		\end{figure}
		
		Na rysunku \ref{fig::cpuvsgpu_1} pokazane jest porównanie liczby operacji stało- i zmiennoprzecinkowych na sekundę dla wybranych procesorów CPU oraz GPU. Tak duża różnica spowodowana jest inną filozofią sprzętową obu procesorów. CPU jest przygotowany do szybkiego wykonywania danego zadania i możliwie najszybszego przejścia do kolejnego, dlatego ma mocną jednostkę arytmetyczno-logiczną ALU (ang. Arithmetic Logic Unit) wzbogaconą o wydajną jednostkę kontrolującą wykonanie programu oraz o mechanizmy optymalizujące zarządzanie pamięcią podręczną (rysunek \ref{fig::cpuvsgpu_2}). Z drugiej strony układy GPU mają zdecydowanie słabszą pojedynczą jednostkę ALU, lecz jest ich na tyle duże, że mogą one przeprowadzić znacznie więcej obliczeń w jednostce czasu. Procesory CPU zoptymalizowane są pod względem jak najkrótszego czasu przetwarzania (ang. latency), zaś układy GPU pod względem ilości obliczeń w danej jednostce czasu (ang. throughput).
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=\linewidth]{img/cpuvsgpu_2.png}
			\caption {Uproszczony schemat funkcjonalny architektury jednostki CPU (po lewej) oraz GPU (po prawej) \cite{cuda}.}
			\label{fig::cpuvsgpu_2}
		\end{figure}
	
	\section{Model programistyczny CUDA}
		\label{sec::model}
		
		Model programistyczny biblioteki zakłada, że procesor CPU jest elementem nadrzędnym -- gospodarzem (ang. host) -- względem jednostki obliczeniowej znajdującej się na karcie graficznej GPU -- urządzeniem (ang. device).
		
		Pierwszym zadaniem gospodarza jest przygotowanie danych do obróbki oraz skopiowanie ich do pamięci urządzenia wykorzystując dostarczane przez CUDA API (ang. Application Programming Interface) funkcje. Następnie procesor CPU wysyła instrukcje wywołania jądra (ang. kernel) obliczeniowego, czyli programu realizowanego w technologii równoległej na procesorze GPU. Obliczenia na urządzeniu wykonywane są asynchronicznie w stosunku do operacji wykonywanych przez CPU, dlatego zachodzi potrzeba synchronizacji obu jednostek. Po przetworzeniu danych następuje kopiowanie pamięci z urządzenia do gospodarza oraz dealokacja pamięci (wszystko inicjowane na rozkaz CPU).
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=0.5\linewidth]{img/grid-of-thread-blocks.png}
			\caption {Schemat organizacji wątków, bloków i siatki bloków według filozofii CUDA \cite{cuda}.}
			\label{fig::cuda_grid}
		\end{figure}
		
		Samo jądro pisane jest, jakby było wykonywane w sposób szeregowy przez pojedynczy wątek (ang. thread). Każde wykonanie jądra realizowane jest przez wiele równoległych wątków, które są uporządkowane w bloki (ang. blocks), które następnie można uporządkować w siatkę (ang. grid). Schemat organizacji wątków, bloków i siatki bloków według filozofii CUDA został pokazany na rysunku \ref{fig::cuda_grid}. Wewnątrz procedury obsługi jądra dostępne są specjalne makra zapewniane przez CUDA API wspomagające kontrolę nad wykonywanym programem.
		
		Powyższy podział został wprowadzony, by ułatwić programiście rozkład problemu na podproblemy i jest oderwany od implementacji sprzętowej i jest poprawny dla każdego typu architektury. Z punktu widzenia sprzętowego podstawowym pojęciem jest multiprocesor strumieniowy SM (ang. streaming multiprocessor). W momencie wywołania jądra obliczeniowego wszystkie bloki wewnątrz siatki bloków zostają ponumerowane i na etapie wykonania rozdzielane są pomiędzy SM, które posiadają wolne zasoby obliczeniowe -- rdzenie CUDA (ang CUDA cores). Najnowsze procesory NVidia GPU są wykonane według specyfikacji architektury Kepler, w której każdy multiprocesor zawiera 192 rdzenie CUDA.
		
		Sam program wykonywany jest sprzętowo w tym samym momencie dla paczki 32 wątków (ang. warps of threads) i jest ona podstawową abstrakcją w implementacji sprzętowej. Wszystkie wątki wchodzące w skład paczki mają wspólny adres początkowy wykonania w programie, lecz posiadają indywidualny licznik instrukcji i rejestr stanu, dzięki któremu są rozróżnialne. Warto zdawać sobie z tego faktu sprawę, ponieważ odpowiednie projektowanie jądra obliczeniowego, w którym paczki wykonują podobne zadania i korzystają z pamięci umieszczonej blisko siebie pozwala osiągnąć lepszą wydajność programu równoległego.
		
		Warto również zaznaczyć, że istnieje możliwość synchronizacji wątków w ramach jednego bloku za pomocą tzw. bariery (ang. barrier). Po dotarciu do bariery dalsze wykonywanie jądra jest wstrzymywane do czasu, aż wszystkie wątki jej nie osiągną. Mechanizm ten pozwala uniknąć niektórych zagrożeń programowania równoległego, między innymi opowiedzianego wcześniej mechanizmu hazardu.

	\section{Hierarchia pamięci CUDA}
	
		Technologia CUDA narzuca odpowiednią hierarchię pamięci. Na urządzeniu każdy wątek ma dostęp do dedykowanych tylko dla niego rejestrów -- jest to bardzo szybka pamięć o wielkości rzędu kilkudziesięciu bajtów na potrzeby przechowywania zmiennych lokalnych. Każdy wątek w ramach jednego bloku ma dostęp do pamięci współdzielonej, która służy do komunikacji między wątkami, ale może być również wykorzystana jako szybka pamięć podręczna bloku. Jej ilość w zależności od urządzenia to kilkadziesiąt kilobajtów, zaś czas dostępu do niej jest zdecydowanie dłuższy niż w przypadku pamięci lokalnej. Każdy wątek wewnątrz każdego bloku ma również dostęp do globalnej pamięci urządzenia, która stanowi największą (do kilku gigabajtów) i jednocześnie najwolniejszą część całkowitej pamięci GPU. W przybliżeniu czas dostępu do różnych typów pamięci komputera można przedstawić w następujący sposób:
		\begin{equation}
			\hbox{p. lokalna} \ll \hbox{p. współdzielona} \ll \hbox{p. globalna GPU} \ll \hbox{p. CPU}.
		\end{equation}
		
		Dobrą praktyką jest przenoszenie często odczytywanych lub zapisywanych zmiennych z wolnych pamięci do pamięci o szybszym dostępie. Świadome wykorzystanie odpowiedniej pamięci pozwala na pisanie bardziej efektywnych programów.
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=0.5\linewidth]{img/memory-hierarchy.png}
			\caption {Hierarchia pamięci według filozofii CUDA \cite{cuda}.}
			\label{fig::cuda_memory_hierarchy}
		\end{figure}
		
	\section{Narzędzia CUDA}
	
		Oprócz samych bibliotek pozwalających na pisanie kodu źródłowego zgodnego ze standardami CUDA firma Nvidia dostarcza również zestaw narzędzi pozwalających ten kod źródłowy skompilować i przetestować. Podstawowe narzędzia to kompilator \textit{nvcc} (ang. Nvidia CUDA Compiler) oraz debugger \textit{cuda-gdb} (ang. CUDA gnu debugger) oraz narzędzie kontroli dostępu do pamięci \textit{cuda-memcheck} (ang. CUDA memory check).
		
		Oprócz podstawowych narzędzi dostępne są inne przygotowane przez firmę dodatki. Użytkownik może skonfigurować bibliotekę CUDA do działania ze swoim ulubionym środowiskiem programistycznym, bądź użyć przygotowanego przez Nvidia środowiska z już skonfigurowaną i gotową do użytku biblioteką - \textit{Nsight Eclipse Edition}. Dostępne jest również narzędzie do dokładnej kontroli czasu, jaki spędzany jest na poszczególnych elementach programu - \textit{CUDA Profiler}. To tylko niektóre z dostępnych narzędzi, które znacznie ułatwiają pracę nad rozwojem programów równoległych w technologii CUDA.